package com.lnsimpson;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class SinglyLinkedListTest
{
    SinglyLinkedList list;

    @BeforeEach
    void setUp()
    {
        list = new SinglyLinkedList();
    }

    @Test
    public void insertNodeAtHead()
    {
        SinglyLinkedListNode newNode = list.insertNodeAtHead(5);
        SinglyLinkedListNode currentHeadNode = list.getHeadNode();
        assertEquals(currentHeadNode.data, 5);
    }

    @Test
    public void insertNodeAtHeadMultiple()
    {
        list.insertNodeAtHead(1);
        list.insertNodeAtHead(2);
        list.insertNodeAtHead(3);
        list.insertNodeAtHead(99);

        SinglyLinkedListNode currentHeadNode = list.getHeadNode();

        //99 is the last one we pushed onto the front of the list, so we check to see if it's there.
        assertEquals(currentHeadNode.data, 99);
    }

    @Test
    public void deleteHeadNode()
    {
        list.insertNodeAtHead(1);
        list.insertNodeAtHead(2);
        list.insertNodeAtHead(3);
        list.insertNodeAtHead(99);

        list.deleteHeadNode();

        SinglyLinkedListNode currentHeadNode = list.getHeadNode();

        //3 is the next to last node we pushed onto the front of the list, so we check to see if it's there.
        assertEquals(currentHeadNode.data, 3);
    }

    @Test
    public void deleteTailNode()
    {
        list.insertNodeAtHead(1);
        list.insertNodeAtHead(2);
        list.insertNodeAtHead(3);
        list.insertNodeAtHead(99);

        list.deleteTailNode();

        SinglyLinkedListNode currentTailNode = list.getTailNode();

        //We loaded the data in via the head, so in this case, we're checking for the 2nd item
        //we pushed, which will be next to the tail in the original list.
        assertEquals(currentTailNode.data, 2);
    }

    @Test
    public void insertNodeAtTail()
    {
        list.insertNodeAtTail(6);
        SinglyLinkedListNode currentTailNode = list.getTailNode();
        assertEquals(currentTailNode.data, 6);
    }

    @Test
    public void insertNodeAtTailMultiple()
    {
        list.insertNodeAtTail(1);
        list.insertNodeAtTail(2);
        list.insertNodeAtTail(3);
        list.insertNodeAtTail(99);

        SinglyLinkedListNode currentTailNode = list.getTailNode();

        //We're gluing these on to the end of the list, so we check for the last one we tacked on there.
        assertEquals(currentTailNode.data, 99);
    }

    @Test
    public void insertNodeAtHeadHighVolume()
    {
        for (int i = 1; i <= 100000; i++)
        {
            list.insertNodeAtHead(i);
        }

        list.insertNodeAtHead(100099);
        SinglyLinkedListNode currentHeadNode = list.getHeadNode();

        //We're gluing these on to the end of the list, so we check for the last one we tacked on there.
        assertEquals(currentHeadNode.data, 100099);
    }

    @Test
    public void insertNodeAtTailHighVolume()
    {
        for (int i = 1; i <= 10000000; i++)
        {
            list.insertNodeAtTail(i);
        }

        list.insertNodeAtTail(10000099);
        SinglyLinkedListNode currentTailNode = list.getTailNode();

        //We're gluing these on to the end of the list, so we check for the last one we tacked on there.
        assertEquals(currentTailNode.data, 10000099);
    }

    @Test
    public void deleteTailNodeHighVolume()
    {
        for (int i = 1; i <= 10000000; i++)
        {
            list.insertNodeAtTail(i);
        }

        list.insertNodeAtTail(10000099);
        list.deleteTailNode();
        SinglyLinkedListNode currentTailNode = list.getTailNode();

        //We're gluing these on to the end of the list, so we check for the last one we tacked on there.
        assertEquals(currentTailNode.data, 10000000);
    }

    @Test
    public void deleteHeadNodeHighVolume()
    {
        for (int i = 1; i <= 100000; i++)
        {
            list.insertNodeAtHead(i);
        }

        list.insertNodeAtHead(100099);
        list.deleteHeadNode();
        SinglyLinkedListNode currentHeadNode = list.getHeadNode();

        //We're gluing these on to the end of the list, so we check for the last one we tacked on there.
        assertEquals(currentHeadNode.data, 100000);
    }

    @Test
    public void deleteNodeByIndex()
    {
        list.insertNodeAtHead(5);
        list.insertNodeAtHead(4);
        list.insertNodeAtHead(3);
        list.insertNodeAtHead(2);
        list.insertNodeAtHead(1);
        list.insertNodeAtHead(0);
        list.deleteNodeByIndex(4);
        SinglyLinkedListNode beforeNode = list.getNodeByIndex(3);
        SinglyLinkedListNode afterNode = list.getNodeByIndex(4);
        assertEquals(beforeNode.data, 3);
        assertEquals(afterNode.data, 5);
    }

    @Test
    public void insertNodeAtPosition()
    {
        list.insertNodeAtHead(5);
        list.insertNodeAtHead(4);
        list.insertNodeAtHead(3);
        list.insertNodeAtHead(2);
        list.insertNodeAtHead(1);
        list.insertNodeAtHead(0);
        list.printElements(list.getHeadNode());
        SinglyLinkedListNode newNode = new SinglyLinkedListNode(99);
        list.insertNodeAtPosition(newNode, 3);
        list.printElements(list.getHeadNode());
        SinglyLinkedListNode beforeNode = list.getNodeByIndex(2);
        SinglyLinkedListNode afterNode = list.getNodeByIndex(4);
        assertEquals(beforeNode.data, 2);
        assertEquals(afterNode.data, 3);
    }

    @Test
    public void getNodeByIndex()
    {
        list.insertNodeAtHead(5);
        list.insertNodeAtHead(4);
        list.insertNodeAtHead(3);
        list.insertNodeAtHead(2);
        list.insertNodeAtHead(1);
        list.insertNodeAtHead(0);
        SinglyLinkedListNode selectedNode = list.getNodeByIndex(4);
        assertEquals(4, selectedNode.data);
    }

    @Test
    public void size()
    {
        list.insertNodeAtHead(5);
        list.insertNodeAtHead(4);
        list.insertNodeAtHead(3);
        list.insertNodeAtHead(2);
        list.insertNodeAtHead(1);
        list.insertNodeAtHead(0);
        assertEquals(6, list.size());
    }

    @Test
    public void reverse()
    {
        list.insertNodeAtHead(5);
        list.insertNodeAtHead(4);
        list.insertNodeAtHead(3);
        list.insertNodeAtHead(2);
        list.insertNodeAtHead(1);
        list.insertNodeAtHead(0);
        SinglyLinkedListNode reversed = list.reverse();
        assertEquals(5, reversed.data);
    }

    @Test
    public void firstIndexOf()
    {
        list.insertNodeAtHead(5);
        list.insertNodeAtHead(4);
        list.insertNodeAtHead(3);
        list.insertNodeAtHead(2);
        list.insertNodeAtHead(1);
        list.insertNodeAtHead(0);
        int foundIndex = list.firstIndexOf(3);
        assertEquals(3, foundIndex);
        foundIndex = list.firstIndexOf(55);
        assertEquals(-1, foundIndex);
        foundIndex = list.firstIndexOf(0);
        assertEquals(0, foundIndex);
        foundIndex = list.firstIndexOf(5);
        assertEquals(5, foundIndex);
    }

    @Test
    public void lastIndexOf()
    {
        list.insertNodeAtHead(5);
        list.insertNodeAtHead(99);
        list.insertNodeAtHead(4);
        list.insertNodeAtHead(3);
        list.insertNodeAtHead(2);
        list.insertNodeAtHead(99);
        list.insertNodeAtHead(1);
        list.insertNodeAtHead(0);
        int foundIndex = list.lastIndexOf(99);
        assertEquals(6, foundIndex);
        foundIndex = list.lastIndexOf(55);
        assertEquals(-1, foundIndex);
        foundIndex = list.lastIndexOf(0);
        assertEquals(0, foundIndex);
        foundIndex = list.lastIndexOf(5);
        assertEquals(7, foundIndex);
    }

    @Test
    public void findOccurrences()
    {
        list.insertNodeAtHead(5);
        list.insertNodeAtHead(99);
        list.insertNodeAtHead(4);
        list.insertNodeAtHead(3);
        list.insertNodeAtHead(2);
        list.insertNodeAtHead(99);
        list.insertNodeAtHead(1);
        list.insertNodeAtHead(0);
        int[] foundValues = list.findOccurrences(99);
        assertEquals(foundValues.length, 2);
        assertEquals(foundValues[0], 2);
        assertEquals(foundValues[1], 6);
        foundValues = list.findOccurrences(44);
        assertEquals(foundValues, null);
    }

    @Test
    public void compare()
    {
        list.insertNodeAtHead(5);
        list.insertNodeAtHead(4);
        list.insertNodeAtHead(3);
        list.insertNodeAtHead(2);
        list.insertNodeAtHead(1);
        list.insertNodeAtHead(0);

        SinglyLinkedList secondList = new SinglyLinkedList();
        secondList.insertNodeAtHead(5);
        secondList.insertNodeAtHead(4);
        secondList.insertNodeAtHead(3);
        secondList.insertNodeAtHead(2);
        secondList.insertNodeAtHead(1);
        secondList.insertNodeAtHead(0);

        boolean listsEqual = list.compare(secondList.getHeadNode());
        assertTrue(listsEqual);
        list.insertNodeAtTail(444);
        listsEqual = list.compare(secondList.getHeadNode());
        assertFalse(listsEqual);
        secondList.insertNodeAtTail(444);
        listsEqual = list.compare(secondList.getHeadNode());
        assertTrue(listsEqual);
        list.insertNodeAtHead(999);
        listsEqual = list.compare(secondList.getHeadNode());
        assertFalse(listsEqual);
        secondList.insertNodeAtHead(999);
        list.deleteNodeByIndex(3);
        listsEqual = list.compare(secondList.getHeadNode());
        assertFalse(listsEqual);
    }

    @Test
    public void sort()
    {
        list.insertNodeAtHead(0);
        list.insertNodeAtHead(3);
        list.insertNodeAtHead(1);
        list.insertNodeAtHead(5);
        list.insertNodeAtHead(2);
        list.insertNodeAtHead(4);
//        list.printElements(list.getHeadNode());
        list.sortAscending();
//        list.printElements(list.getHeadNode());
        assertEquals(list.getHeadNode().data, 0);
        assertEquals(list.getTailNode().data, 5);
    }

    @Test
    public void removeDuplicatesFromSorted()
    {
        list.insertNodeAtHead(0);
        list.insertNodeAtHead(1);
        list.insertNodeAtHead(1);
        list.insertNodeAtHead(3);
        list.insertNodeAtHead(2);
        list.insertNodeAtHead(1);
        list.insertNodeAtHead(5);
        list.insertNodeAtHead(4);
        list.insertNodeAtHead(1);
        list.insertNodeAtHead(2);
        list.insertNodeAtHead(4);
        list.sortAscending();
        list.removeDuplicatesFromSorted();
        list.printElements(list.getHeadNode());
        assertEquals(list.size(), 6);
        assertEquals(list.getNodeByIndex(3).data, 3);
    }


}