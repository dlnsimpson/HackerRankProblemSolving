package com.lnsimpson;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DoublyLinkedListTest
{
    DoublyLinkedList list = new DoublyLinkedList();

    @BeforeEach
    void setUp()
    {
        list = new DoublyLinkedList();
    }

    @AfterEach
    void tearDown()
    {
    }

    @Test
    void insertNodeAtHead()
    {
        list.insertNodeAtHead(4);
        assertEquals(list.getHeadNode().data, 4);
    }
}