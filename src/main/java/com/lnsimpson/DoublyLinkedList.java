package com.lnsimpson;

class DoublyLinkedListNode
{
    public int data;
    public DoublyLinkedListNode next;
    public DoublyLinkedListNode prev;

    public DoublyLinkedListNode(int nodeData)
    {
        this.data = nodeData;
        this.next = null;
        this.prev = null;
    }
}

public class DoublyLinkedList
{
    public DoublyLinkedListNode head;
    public DoublyLinkedListNode tail;

    public DoublyLinkedList()
    {
        this.head = null;
        this.tail = null;
    }
    /**
     * This method inserts a new node with the specified data at the head of the linked list.
     * <p>
     * O(1) execution time because we don't have to traverse the whole list.
     *
     * @param data The node's data
     * @return The head node of the list
     */
    public DoublyLinkedListNode insertNodeAtHead(int data)
    {
        DoublyLinkedListNode newNode = new DoublyLinkedListNode(data);
        DoublyLinkedListNode original = head;
        if (original == null)
        {
            //We have an empty list, so this new node is both head & tail
            tail = newNode;
            newNode.prev = null;
        }
        else
        {
            //There's already stuff in the list, so we just set our new node's
            //next value to the current head node.
            newNode.prev = null;
            original.prev = newNode;
            newNode.next = original;
        }

        //The new node is stored in the head node variable.
        head = newNode;
        return head;
    }

    public DoublyLinkedListNode getHeadNode()
    {
        return head;
    }


}
