package com.lnsimpson;

import java.util.Arrays;

class SinglyLinkedListNode
{
    public int data;
    public SinglyLinkedListNode next;

    public SinglyLinkedListNode(int nodeData)
    {
        this.data = nodeData;
        this.next = null;
    }
}

/**
 * A very basic example of a single linked list.
 * <p>
 * These correspond to the Single Linked List exercises at Hacker Rank.
 */
public class SinglyLinkedList
{
    public SinglyLinkedListNode head;
    public SinglyLinkedListNode tail;
    SinglyLinkedListNode sorted = null;

    public SinglyLinkedList()
    {
        this.head = null;
        this.tail = null;
    }

    /**
     * This method inserts a new node with the specified data at the head of the linked list.
     * <p>
     * O(1) execution time because we don't have to traverse the whole list.
     *
     * @param data The node's data
     * @return The head node of the list
     */
    public SinglyLinkedListNode insertNodeAtHead(int data)
    {
        SinglyLinkedListNode newNode = new SinglyLinkedListNode(data);
        if (head == null)
        {
            //We have an empty list, so this new node is both head & tail
            tail = newNode;
        }
        else
        {
            //There's already stuff in the list, so we just set our new node's
            //next value to the current head node.
            newNode.next = head;
        }

        //The new node is stored in the head node variable.
        head = newNode;
        return head;
    }

    /**
     * This method inserts a new node containing the specified data at the tail of the linked list.
     * <p>
     * O(1) execution time because we don't have to traverse the whole list.
     *
     * @param data The node's data
     * @return The list's head node
     */
    public SinglyLinkedListNode insertNodeAtTail(int data)
    {
        SinglyLinkedListNode newNode = new SinglyLinkedListNode(data);
        if (tail == null)
        {
            //We have an empty list, so the node we just created is both head & tail.
            head = newNode;
        }
        else
        {
            tail.next = newNode;
        }
        tail = newNode;
        return head;
    }

    /**
     * This method returns the current tail node.
     *
     * @return The list's tail node;
     */
    public SinglyLinkedListNode getTailNode()
    {
        return tail;
    }

    /**
     * This method returns the current head node.
     *
     * @return The list's head node;
     */
    public SinglyLinkedListNode getHeadNode()
    {
        return head;
    }

    /**
     * This method disconnects the current head node and points instead to the next one down the chain.
     */
    public void deleteHeadNode()
    {
        if (head == null)
        {
            return;
        }
        else
        {
            if (head == tail)
            {
                //This is the last node in the list, so now everything's null.
                head = null;
                tail = null;
                return;
            }
            else
            {
                //Point the head variable to the current head's next node.
                head = head.next;
            }
        }
    }

    /**
     * This method removes the tail node and sets the next to last node as the new tail.
     * <p>
     * O(n) because we have to traverse the list to find the tail node's upstream neighbor.
     */
    public void deleteTailNode()
    {

        if (head == tail)
        {
            //This is the last node in the list, so now everything's null.
            head = null;
            tail = null;
        }
        else
        {
            SinglyLinkedListNode predecessor = head;

            while (predecessor.next != tail)
            {
                predecessor = predecessor.next;
            }
            tail = predecessor;
            tail.next = null;
        }
    }

    /**
     * This method runs through the list until it hits the index position, then it deletes that node;
     * <p>
     * The search part of the operation is O(n) because we may have to go through the whole thing to find the node we want.
     * The delete is O(1) because all we have to do is hook together the nodes before and after the one being deleted.
     *
     * @param index The position, starting with head node = 0, of the node to be deleted.
     */
    public void deleteNodeByIndex(int index)
    {
        SinglyLinkedListNode currentNode = head;
        SinglyLinkedListNode previousNode = head;

        if (head == null)
        {
            return;
        }

        for (int i = 0; i < index; i++)
        {
            previousNode = currentNode;
            currentNode = currentNode.next;
        }
        previousNode.next = currentNode.next;
    }

    /**
     * This method runs through the list until it hits the index position, then it returns that node;
     * <p>
     * The search part of the operation is O(n) because we may have to go through the whole thing to find the node we want.
     *
     * @param index The position, starting with head node = 0, of the node to be returned.
     */
    public SinglyLinkedListNode getNodeByIndex(int index)
    {
        SinglyLinkedListNode currentNode = head;

        for (int i = 0; i < index; i++)
        {
            currentNode = currentNode.next;
        }
        return currentNode;
    }

    /**
     * This method skips forward to the insertion point designated by the index and inserts the new node.
     * <p>
     * The operation is O(n) because may we have to go through the whole list.
     *
     * @param newNode The new node we are inserting
     * @param index   The position where the node will be inserted
     */
    public void insertNodeAtPosition(SinglyLinkedListNode newNode, int index)
    {
        SinglyLinkedListNode currentNode = head;
        SinglyLinkedListNode previousNode = null;

        for (int i = 0; i < index; i++)
        {
            previousNode = currentNode;
            currentNode = currentNode.next;
        }
        previousNode.next = newNode;
        newNode.next = currentNode;
    }

    /**
     * This method runs through the list of nodes and prints each value.
     * <p>
     * The operation is O(n) because we have to go through the whole list.
     *
     * @param node The head node for the list we are printing
     */
    public void printElements(SinglyLinkedListNode node)
    {
        if (node != null)
        {
            System.out.println(node.data);
            printElements(node.next);
        }
    }

    /**
     * This method runs through the list of nodes and counts them.
     * <p>
     * The operation is O(n) because we have to go through the whole list.
     *
     * @return size An int containing the number of elements.
     */
    public int size()
    {
        SinglyLinkedListNode currentNode = head;
        int counter = 0;
        while (currentNode != null)
        {
            currentNode = currentNode.next;
            counter++;
        }
        return counter;
    }

    /**
     * This method runs through the list of nodes and reverses them.
     * <p>
     * The operation is O(n) because we have to go through the whole list.
     *
     * @return size The reversed list.
     */
    public SinglyLinkedListNode reverse()
    {
        SinglyLinkedListNode previousNode = null;
        SinglyLinkedListNode currentNode = head;
        SinglyLinkedListNode nextNode = null;
        while (currentNode != null)
        {
            nextNode = currentNode.next;
            currentNode.next = previousNode;
            previousNode = currentNode;
            currentNode = nextNode;
        }
        head = previousNode;
        return head;
    }

    /**
     * This method runs through the list of nodes looking for the first index of the data.
     * <p>
     * The operation is O(n) because we may have to go through the whole list.
     *
     * @param value The value to search for
     * @return int The index of the first occurrence of the data.
     */
    public int firstIndexOf(int value)
    {
        SinglyLinkedListNode currentNode = head;
        int counter = 0;

        while (currentNode != null)
        {
            if (currentNode.data == value)
            {
                return counter;
            }
            else
            {
                currentNode = currentNode.next;
                counter++;
            }
        }
        return -1;
    }

    /**
     * This method runs through the list of nodes looking for the last index of the data.
     * <p>
     * The operation is O(n) because we have to go through the whole list.
     *
     * @param value The value to search for
     * @return int The index of the last occurrence of the data.
     */
    public int lastIndexOf(int value)
    {
        SinglyLinkedListNode currentNode = head;
        int counter = 0;
        int foundIndex = -1;

        while (currentNode != null)
        {
            if (currentNode.data == value)
            {
                foundIndex = counter;
            }
            counter++;
            currentNode = currentNode.next;
        }
        return foundIndex;
    }

    /**
     * This method runs through the list of nodes looking for all of the occurrences of the data;
     * <p>
     * The operation is O(n) because we have to go through the whole list.
     *
     * @param value The value to search for
     * @return int[] An array containing the indices of the found values
     */
    public int[] findOccurrences(int value)
    {
        StringBuilder sb = new StringBuilder();
        SinglyLinkedListNode currentNode = head;
        int counter = 0;
        int[] values;

        while (currentNode != null)
        {
            if (currentNode.data == value)
            {
                sb.append(counter).append(" ");
            }

            counter++;
            currentNode = currentNode.next;
        }
        //This is a pretty odd way to go about building the int array, but
        //I wasn't in the mood to deal with building an array with a loop.
        if (sb.length() > 0)
        {
            String[] strValues = sb.toString().split(" ");
            values = Arrays.asList(strValues).stream().mapToInt(Integer::parseInt).toArray();
        }
        else
        {
            values = null;
        }
        return values;
    }

    /**
     * This method compares the current list to another one.
     * <p>
     * The operation is O(n) because we may have to go through the whole list.
     *
     * @param comparedNode The node we're comparing to
     * @return boolean Whether the nodes are equal or not
     */
    public boolean compare(SinglyLinkedListNode comparedNode)
    {
        SinglyLinkedListNode current = head;
        while (current != null && comparedNode != null)
        {
            if (current.data != comparedNode.data)
            {
                //The first time we hit a mismatch, we jump out and return false.
                return false;
            }
            current = current.next;
            comparedNode = comparedNode.next;
        }
        //Now we check to make sure we've hit null for both lists. If not, we've run out of
        //nodes in one but not the other, in which case the lists aren't equal.
        return (current == null && comparedNode == null);
    }

    /**
     * This method does an ascending sort on the list
     * <p>
     * The operation is not optimal at all and needs to be refactored. Included
     * here only because I needed a sorted Linked List for some of the Hacker Rank exercises.
     */
    public SinglyLinkedListNode sortAscending()
    {
        SinglyLinkedListNode current = head;
        SinglyLinkedListNode keeper = null;
        int holder = -1;

        //If the head node is null, we have no work to do, so return.
        if (head == null)
        {
            return null;
        }
        else
        {
            //We loop through the list until we hit the end.
            while (current != null)
            {
                //We store the next node in a variable.
                keeper = current.next;

                while (keeper != null)
                {
                    if (current.data > keeper.data)
                    {
                        holder = current.data;
                        current.data = keeper.data;
                        keeper.data = holder;
                    }
                    keeper = keeper.next;
                }
                current = current.next;
            }
        }
        return current;
    }

    /**
     * This method removes duplicate items from the linked list
     * <p>
     * It's O(n) because we have to walk the entire list.
     */
    public void removeDuplicatesFromSorted()
    {
        SinglyLinkedListNode current = head;
        while (current != null && current.next != null)
        {
            if (current.next.data == current.data)
            {
                //We've found a duplicate, so we route around it.
                current.next = current.next.next;
            }
            else
            {
                //No duplicate, so we just move on.
                current = current.next;
            }
        }
    }

//    public SinglyLinkedListNode findMergeNode(SinglyLinkedListNode secondary)
//    {
//        SinglyLinkedListNode part1 = head;
//        SinglyLinkedListNode part2 = secondary;
//
//        while (part1 != part2)
//        {
//            part1 = (part1 != null) ? part1.next : secondary;
//            part2 = (part2 != null) ? part2.next : head;
//        }
//        return part1;
//    }
}
