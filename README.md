# HackerRankProblemSolving
 
 I ran through a number of puzzles on HackerRank in 2019, but I'm not sure that my actual solutions are publicly visible. I decided to reproduce the solutions here, but with JUnit tests instead of the test harness Hacker Rank uses.
 
 As of this writing (8/29/2020), I'm just sort of noodling around getting the solutions translated into a form that can be run natively in IntelliJ IDEA and tested with JUnit. I may not have a full suite of test cases like the ones on Hacker Rank, at least not for now.    
